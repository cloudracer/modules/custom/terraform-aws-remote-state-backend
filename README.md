# Terraform AWS Remote State Backend

## Overview

This Module create a Terraform Remote State Backend with S3 Bucket and DynamoDB Table

## Usage

````
module "backend" {
  source  = "git@gitlab.com:tecracer-intern/terraform-landingzone/modules/terraform-aws-remote-state-backend.git?ref=0.0.1"

  organisation = <Organization>
  system       = <System>
  tags         = <Tags>
}
````

## Contributing
This module is intended to be a shared module.
Please don't commit any customer-specific configuration into this module and keep in mind that changes could affect other projects.

For new features please create a branch and submit a pull request to the maintainers.

Accepted new features will be published within a new release/tag.

## Pre-Commit Hooks

### Enabled hooks
- id: end-of-file-fixer
- id: trailing-whitespace
- id: terraform_fmt
- id: terraform_docs
- id: terraform_validate
- id: terraform_tflint

<!-- BEGINNING OF PRE-COMMIT-TERRAFORM DOCS HOOK -->
## Requirements

| Name | Version |
|------|---------|
| <a name="requirement_terraform"></a> [terraform](#requirement\_terraform) | >= 0.13 |
| <a name="requirement_aws"></a> [aws](#requirement\_aws) | >= 3.0 |

## Providers

| Name | Version |
|------|---------|
| <a name="provider_aws"></a> [aws](#provider\_aws) | 4.2.0 |
| <a name="provider_random"></a> [random](#provider\_random) | 3.1.0 |

## Modules

No modules.

## Resources

| Name | Type |
|------|------|
| [aws_dynamodb_table.terraform_dynamodb](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/dynamodb_table) | resource |
| [aws_s3_bucket.terraform_bucket](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/s3_bucket) | resource |
| [aws_s3_bucket_policy.delete_protection](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/s3_bucket_policy) | resource |
| [aws_s3_bucket_public_access_block.main](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/s3_bucket_public_access_block) | resource |
| [random_integer.random](https://registry.terraform.io/providers/hashicorp/random/latest/docs/resources/integer) | resource |

## Inputs

| Name | Description | Type | Default | Required |
|------|-------------|------|---------|:--------:|
| <a name="input_organisation"></a> [organisation](#input\_organisation) | Name of the current organisation | `string` | n/a | yes |
| <a name="input_system"></a> [system](#input\_system) | Name of a dedicated system or application | `string` | n/a | yes |
| <a name="input_tags"></a> [tags](#input\_tags) | Tag for all resources | `map(string)` | `{}` | no |

## Outputs

| Name | Description |
|------|-------------|
| <a name="output_tf_state_dynamodb_arn"></a> [tf\_state\_dynamodb\_arn](#output\_tf\_state\_dynamodb\_arn) | n/a |
| <a name="output_tf_state_dynamodb_name"></a> [tf\_state\_dynamodb\_name](#output\_tf\_state\_dynamodb\_name) | n/a |
| <a name="output_tf_state_s3_bucket_arn"></a> [tf\_state\_s3\_bucket\_arn](#output\_tf\_state\_s3\_bucket\_arn) | n/a |
| <a name="output_tf_state_s3_bucket_name"></a> [tf\_state\_s3\_bucket\_name](#output\_tf\_state\_s3\_bucket\_name) | n/a |
<!-- END OF PRE-COMMIT-TERRAFORM DOCS HOOK -->
